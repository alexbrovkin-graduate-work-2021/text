\section{Автоматизация процесса}
Эта глава посвящена автоматизации процесса обработки запроса на обновление конфигурации. В ней описывается построение архитектуры приложения, рассматриваются интеграции со сторонними сервисами и некоторые ключевые моменты программного кода.

\subsection{Обзор архитектуры}
После построения модели процесса возникает вопрос, как сделать его исполняемым, а именно "--- как выполнять сторонний код. Автоматизация может пойти разными путями. Существует несколько способов развертывания Camunda Platform. Согласно официальным рекомендациям, лучше всего разрабатывать проект автоматизации процесса как Spring Boot приложение~\cite[Using a Greenfield Stack]{camundabestpract}, подключая в качестве библиотек стартеры Camunda Spring Boot, предоставляющие API для взаимодействия приложения с процессным движком и веб-приложения Camunda.

Spring Boot~\cite{heckler2021spring} "--- это набор утилит, упрощающих процесс разработки, конфигурирования и развертки приложений на основе Spring Framework. При написании данной работы используется актуальная версия 2.4.5. Для разработки применяется Java 11~\cite{samoylov2018java}.

Опишем архитектуру проекта. За сборку и запуск отвечает система автоматической сборки Gradle~\cite{springgradle245docs}. Spring Boot приложение по умолчанию запускается на встроенном сервере Apache Tomcat~\cite[Chapter~3]{spring245docs}. Само приложение содержит в себе:
\begin{itemize}
	\item сервисы "--- различные интеграции, отвечающие за логику приложения;
	\item делегаты "--- классы, вызываемые во время выполнения процесса;
	\item простые Java-объекты "--- классы, моделирующие различные сущности;
	\item контроллеры "--- реализация REST (\textit{Representational State Transfer} "--- передача состояния представления) API, позволяющая обращаться к приложению путем HTTP-запросов (\textit{HyperText Transfer Protocol} "--- протокол передачи гипертекста);
	\item конфигурации "--- классы, отвечающие за установку соединения с СУБД или подготовку клиентов Jira и GitLab; 
	\item Data Access Object (объект доступа к данным) "--- интерфейс к СУБД;
	\item встроенные ресурсы: файлы BPMN-моделей и HTML-форма (\textit{HyperText Markup Language} "--- язык гипертекстовой разметки) пользовательской задачи процесса.
\end{itemize}

Интеграции с Jira и GitLab реализуются путем использования сторонних библиотек, существующих в открытом доступе~\cite{jiraapi, gitlab4japi}.

\subsubsection{О взаимодействии с Jira}
Функционал подключаемой библиотеки используется для, например, отправки к заявке комментария или ее закрытия. Однако он не позволяет подписываться на события, что необходимо для запуска выполнения процесса при создании новой заявки. Для этого случая Jira предоставляет возможность использовать технологию вебхуков~\cite[Chapter~9]{kuruvilla2016jira}. Вебхук "--- это инструмент уведомления клиента сервером о происходящих событиях посредством функции обратного вызова, предоставляемой клиентом. В данном случае это отправка HTTP-запроса по заранее установленному адресу. Существует одно ограничение: поддерживается только протокол HTTPS (\textit{HTTP Secure} "--- безопасный HTTP) и порт 443. То есть необходима возможность шифрования трафика, идущего от Jira к приложению.

Таким образом, готовое приложение потребуется запускать на веб-сервере с дополнительно установленным SSL-сертификатом (\textit{Secure Sockets Layer} "--- уровень защищенных сокетов). Для выпуска доверенного сертификата необходимо, чтобы серверу был назначен публичный статический IP-адрес (\textit{Internet Protocol} "--- межсетевой протокол). В качестве сервера выбран Apache HTTP Server, установленный и запущенный на виртуальной машине с операционной системой Debian.

В результате была составлена схема, представленная на рисунке~\ref{fig:app:architecture}, которая демонстрирует архитектуру решения: приложение, содержащее код автоматизации и Camunda Platform, запускаемое на порте 8080 при помощи Apache Tomcat; Apache HTTP Server, принимающий зашифрованный трафик через порт 443 и перенаправляющий его к приложению на порт 8080; виртуальная машина с Debian, предназначенная для развертывания.

\begin{figure}[h]
	\begin{center}
%		\includesvg[width=\textwidth]{automation/architecture2.svg}
		\includegraphics[width=\textwidth]{automation/architecture.pdf}
		\caption{Архитектура проекта автоматизации}
		\label{fig:app:architecture}
	\end{center}
\end{figure}


\subsection{Интеграция с Jira}
Для реализации необходимых методов используется библиотека \texttt{jira-rest-java-client-core}~\cite{jiraapi} от Atlassian "--- разработчика Jira. Она предоставляет класс \texttt{IssueRestClient}, позволяющий взаимодействовать с заявками. Чтобы получить готовый для работы клиент, требуется реализовать класс конфигурации, создающий экземпляр клиента на основе адреса сервера Jira и данных для авторизации. Код представлен в листинге~\ref{lst:JiraRestClientConfig}. Конфигурация помечается аннотацией \texttt{Configuration}. Аннотация \texttt{Value} используется для того, чтобы получить значение из свойств приложения и присвоить его соответствующей переменной.

\begin{listing}[H]
	\begin{minted}{java}
@Configuration
public class JiraRestClientConfig {
	@Value("${jira.url}")
	private String |\p{url}|;
	@Value("${jira.username}")
	private String |\p{username}|;
	@Value("${jira.api-token}")
	private String |\p{apiToken}|;
	
	@Bean
	public IssueRestClient issueRestClient() {
		JiraRestClient jiraRestClient = 
			new AsynchronousJiraRestClientFactory()
				.createWithBasicHttpAuthentication(URI.|\st{create}|(|\p{url}|),
					|\p{username}|, |\p{apiToken}|);
		return jiraRestClient.getIssueClient();
	}
}
	\end{minted}
	\caption{Конфигурация REST-клиента Jira}
	\label{lst:JiraRestClientConfig} 
\end{listing}

Конкретная заявка описывается классом \texttt{Issue}. Для работы с заявками реализован класс \texttt{JiraIntegrationService} (см. приложение~\ref{jiraintegrationsrc}), содержащий методы для получения заявки по ключу, отправки к ней комментария, а также изменения статуса заявки.


\subsection{Формирование запроса}
Для получения полей из заявки был реализован перечисляемый тип, представленный в листинге~\ref{lst:ConfigurationUpdateFieldType}.

Класс \texttt{ConfigurationUpdateRequest} из пакета \texttt{automation.domain} описывает запрос на обновление конфигурации. Для формирования запроса из заявки разработан метод, представленный в приложении~\ref{requestofissuesrc}. В нем, помимо прочего, обозначен список обязательных полей заявки, а также используются два паттерна регулярных выражений: один предназначен для проверки на корректность строки с перечислением колонок, второй позволяет получать для каждой колонки отдельно имя и тип с длиной (см. листинг~\ref{lst:patterns}).

\begin{listing}[H]
	\begin{minted}{java}
public enum ConfigurationUpdateFieldType {
	|\const{SCHEMA}|("Schema name"),
	|\const{TABLE}|("Table name"),
	|\const{COLUMNS}|("Set of columns"),
	|\const{FREQUENCY}|("Frequency"),
	|\const{CREATE\_SNAPSHOTS}|("Create snapshots"),
	|\const{UNWANTED\_FIELD}|("");
	
	private final String fieldTypeName;
	
	public static ConfigurationUpdateFieldType fromJiraName(String name) {
		for (ConfigurationUpdateFieldType type : ConfigurationUpdateFieldType.|\st{values()}|) {
			if (type.|\p{fieldTypeName}|.equalsIgnoreCase(name)) {
				return type;
			}
		}
		return |\const{UNWANTED\_FIELD}|;
	}
	
	|\textcolor{static}{ConfigurationUpdateFieldType}|(String fieldTypeName) {
		this.|\p{fieldTypeName}| = fieldTypeName;
	}
}
	\end{minted}
	\caption{Перечисляемый тип с названиями полей Jira}
	\label{lst:ConfigurationUpdateFieldType} 
\end{listing}

\begin{listing}[H]
	\begin{minted}[breakafter=*]{java}
private static final List<ConfigurationUpdateFieldType> REQUIRED_FIELDS = List.|\st{of}|(|\const{SCHEMA}|, |\const{TABLE}|, |\const{FREQUENCY}|, |\const{CREATE\_SNAPSHOTS}|);

private static final Pattern |\const{COLUMNS\_FIELD\_CORRECT\_PATTERN}| =
	Pattern.|\st{compile}|(
		"^\\s*(?:\\w+\\s*:(?:\\w|\\s)*\\w+\\s*(?:|\\(\\s*\\d+\\s*\\)\\s*)(?:,\\s*(?!$)|$))+$"
	);
private static final Pattern |\const{COLUMNS\_ITEMS\_PATTERN}| =
	Pattern.|\st{compile}|(
		"(?<name>\\w+)\\s*:\\s*(?<type>(?:\\w\\s*)*\\w+)\\s*(?:\\(\\s*(?<length>\\d+)\\s*\\))?"
	);
	\end{minted}
	\caption{Регулярные выражения для проверки и обработки поля с колонками}
	\label{lst:patterns}
\end{listing}


\subsection{Запросы к СУБД}
Необходимо сконфигурировать два источника данных: первый "--- это база данных, необходимая для работы Camunda Platform, второй "--- непосредственно хранилище, из которого производится выгрузка. Класс конфигурации представлен в листинге~\ref{lst:DataSourceConfig}.

\begin{longlisting}
	\begin{minted}{java}
@Configuration
public class DataSourceConfig {
	@Bean("camundaBpmDataSource")
	@ConfigurationProperties("datasource.camunda")
	public DataSource camundaBpmDataSource() {
		return DataSourceBuilder.|\st{create}|().type(HikariDataSource.|\class|).build();
	}
	
	@Bean("storageDataSource")
	@Primary
	@ConfigurationProperties("datasource.storage")
	public DataSource storageDataSource() {
		return DataSourceBuilder.|\st{create}|().type(HikariDataSource.|\class|).build();
	}
}
	\end{minted}
	\caption{Конфигурация источников данных}
	\label{lst:DataSourceConfig}
\end{longlisting}

Листинг~\ref{lst:DatabaseDao} демонстрирует интерфейс, предназначенный для отправки запросов к СУБД. Описаны методы для проверки существования схемы, существования таблицы в схеме и получения описания колонок для указанной таблицы.

\begin{longlisting}
	\begin{minted}{java}
@Mapper
public interface DatabaseDao {
	
	@Select(
		"SELECT exists(SELECT FROM information_schema.schemata WHERE schema_name = #{schemaName})"
	)
	boolean schemaExists(String schemaName);
	
	@Select(
		"SELECT exists(SELECT FROM information_schema.tables WHERE table_schema = #{schemaName} AND table_name = #{tableName})"
	)
	boolean tableExists(String schemaName, String tableName);
	
	@Select(
		"SELECT column_name as columnName, data_type as dataType, character_maximum_length as charMaxLen FROM information_schema.columns WHERE table_schema = #{schemaName} AND table_name = #{tableName}"
	)
	Set<Column> tableDescription(String schemaName, String tableName);
}
	\end{minted}
	\caption{Интерфейс взаимодействия с СУБД}
	\label{lst:DatabaseDao}
\end{longlisting}


\subsection{Интеграция с GitLab}
Для взаимодействия используется библиотека \texttt{gitlab4j-api}~\cite{gitlab4japi}. Первоначально нужно настроить конфигурацию классов \texttt{GitLabApi} и \texttt{GitLabProperties}, как указано в листинге~\ref{lst:GitLabApiConfig}.

\begin{longlisting}
	\begin{minted}{java}
@Configuration
public class GitLabApiConfig {
	
	@Value("${gitlab.host-url}")
	private String |\p{hostUrl}|;
	@Value("${gitlab.access-token}")
	private String |\p{accessToken}|;
	@Value("${gitlab.repo-path}")
	private String |\p{repoPath}|;
	@Value("${gitlab.start-branch}")
	private String |\p{startBranch}|;
	
	@Bean
	public GitLabProperties gitLabProperties() {
		return new GitLabProperties(|\p{hostUrl}|, |\p{accessToken}|, |\p{repoPath}|, |\p{startBranch}|);
	}
	
	@Bean
	public GitLabApi gitLabApi() {
		return new GitLabApi(gitLabProperties().getHostUrl(), gitLabProperties().getAccessToken());
	}
}
	\end{minted}
	\caption{Конфигурация GitLab API}
	\label{lst:GitLabApiConfig}
\end{longlisting}

Реализованы методы, выполняющие загрузку файла в репозиторий (создание или обновление в зависимости от существования файла), создание запроса на слияние, проверку статуса запроса на слияние и преобразование строки в корректное название ветки. Исходный код интеграции находится в приложении~\ref{gitlabintegrationsrc}.


\subsection{Автоматизация процесса}
Подавляющее большинство задач в процессе являются сервисными задачами. Вызов Java-кода из сервисной задачи возможен при помощи классов-делегатов, которые реализуют интерфейс \texttt{JavaDelegate} с единственным методом \texttt{execute}, принимающим в качестве параметра объект класса \texttt{DelegateExecution}~\cite[Reference / Service Task]{camunda715docs}. Этот объект описывает текущее состояние выполнения экземпляра процесса и позволяет, к примеру, управлять переменными процесса. Каждый класс-делегат должен быть помечен аннотацией \texttt{Component}. Делегаты находятся в пакете \texttt{automation.delegate}. Листинг~\ref{lst:BuildRequestDelegate} демонстрирует один из множества таких классов, реализованных для автоматизации процесса, в качестве примера.

\begin{listing}[h]
	\begin{minted}{java}
@Component("buildRequestDelegate")
public class BuildRequestDelegate implements JavaDelegate {
	@Autowired
	private JiraIntegrationService |\p{jiraIntegrationService}|;
	
	@Override
	public void execute(DelegateExecution execution) {
		ProcessVariables variables = new ProcessVariables(execution);
		String issueKey = variables.getIssueKey();
		
		|\p{jiraIntegrationService}|.setIssueInProgress(issueKey);
		ConfigurationUpdateRequest request = ConfigurationUpdateRequest.|\st{of}|(|\p{jiraIntegrationService}|.getIssue(issueKey));
		
		variables.setRequest(request);
		variables.setValidationStatus(new ValidationStatus());
	}
}
	\end{minted}
	\caption{Пример делегата}
	\label{lst:BuildRequestDelegate}
\end{listing}

Исходный код разработанного приложения представлен в репозитории~\cite{projectsource}.
