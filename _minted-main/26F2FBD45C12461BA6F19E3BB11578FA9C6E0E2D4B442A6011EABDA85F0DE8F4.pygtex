\begin{Verbatim}[commandchars=\\\{\},codes={\catcode`\$=3\catcode`\^=7\catcode`\_=8\relax}]
\PYG{k+kn}{package} \PYG{n+nn}{automation.service}\PYG{p}{;}

\PYG{k+kn}{import} \PYG{n+nn}{automation.domain.GitLabProperties}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.gitlab4j.api.GitLabApi}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.gitlab4j.api.GitLabApiException}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.gitlab4j.api.models.CommitAction}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.gitlab4j.api.models.CommitPayload}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.gitlab4j.api.models.MergeRequest}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.gitlab4j.api.models.MergeRequestParams}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.slf4j.Logger}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.slf4j.LoggerFactory}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.springframework.beans.factory.annotation.}\PYG{esc}{\Autowired}\PYG{p}{;}
\PYG{k+kn}{import} \PYG{n+nn}{org.springframework.stereotype.}\PYG{esc}{\Service}\PYG{p}{;}

\PYG{n+nd}{@Service}
\PYG{k+kd}{public} \PYG{k+kd}{class} \PYG{n+nc}{GitLabIntegrationService} \PYG{p}{\PYGZob{}}
	
	\PYG{k+kd}{private} \PYG{k+kd}{static} \PYG{k+kd}{final} \PYG{n}{Logger} \PYG{esc}{\LOGGER} \PYG{o}{=} \PYG{n}{LoggerFactory}\PYG{p}{.}\PYG{n+na}{getLogger}\PYG{p}{(}\PYG{n}{GitLabIntegrationService}\PYG{p}{.}\PYG{n+na}{class}\PYG{p}{)}\PYG{p}{;}
	
	\PYG{n+nd}{@Autowired}
	\PYG{k+kd}{private} \PYG{n}{GitLabApi} \PYG{esc}{\gitLabApi}\PYG{p}{;}
	\PYG{n+nd}{@Autowired}
	\PYG{k+kd}{private} \PYG{n}{GitLabProperties} \PYG{esc}{\gitLabProperties}\PYG{p}{;}
	
	\PYG{k+kd}{public} \PYG{k+kt}{void} \PYG{n+nf}{commitFile}\PYG{p}{(}\PYG{n}{String} \PYG{n}{branchName}\PYG{p}{,} \PYG{n}{String} \PYG{n}{commitMessage}\PYG{p}{,} \PYG{n}{String} \PYG{n}{fileName}\PYG{p}{,} \PYG{n}{String} \PYG{n}{fileContent}\PYG{p}{)} \PYG{k+kd}{throws} \PYG{n}{GitLabApiException} \PYG{p}{\PYGZob{}}
		\PYG{esc}{\LOGGER}\PYG{p}{.}\PYG{n+na}{info}\PYG{p}{(}\PYG{l+s}{\PYGZdq{}}\PYG{l+s}{Committing file \PYGZsq{}\PYGZob{}\PYGZcb{}\PYGZsq{} to branch \PYGZsq{}\PYGZob{}\PYGZcb{}\PYGZsq{}}\PYG{l+s}{\PYGZdq{}}\PYG{p}{,} \PYG{n}{fileName}\PYG{p}{,} \PYG{n}{branchName}\PYG{p}{)}\PYG{p}{;}
		\PYG{k+kt}{boolean} \PYG{n}{fileExists}\PYG{p}{;}
		\PYG{k}{try} \PYG{p}{\PYGZob{}}
			\PYG{esc}{\gitLabApi}\PYG{p}{.}\PYG{n+na}{getRepositoryFileApi}\PYG{p}{(}\PYG{p}{)}\PYG{p}{.}\PYG{n+na}{getFile}\PYG{p}{(}\PYG{esc}{\gitLabProperties}\PYG{p}{.}\PYG{n+na}{getRepoPath}\PYG{p}{(}\PYG{p}{)}\PYG{p}{,} \PYG{n}{fileName}\PYG{p}{,} \PYG{esc}{\gitLabProperties}\PYG{p}{.}\PYG{n+na}{getStartBranch}\PYG{p}{(}\PYG{p}{)}\PYG{p}{)}\PYG{p}{;}
			\PYG{n}{fileExists} \PYG{o}{=} \PYG{k+kc}{true}\PYG{p}{;}
			\PYG{esc}{\LOGGER}\PYG{p}{.}\PYG{n+na}{info}\PYG{p}{(}\PYG{l+s}{\PYGZdq{}}\PYG{l+s}{File \PYGZsq{}\PYGZob{}\PYGZcb{}\PYGZsq{} already exists and will be updated}\PYG{l+s}{\PYGZdq{}}\PYG{p}{,} \PYG{n}{fileName}\PYG{p}{)}\PYG{p}{;}
		\PYG{p}{\PYGZcb{}} \PYG{k}{catch} \PYG{p}{(}\PYG{n}{GitLabApiException} \PYG{n}{e}\PYG{p}{)} \PYG{p}{\PYGZob{}}
			\PYG{k}{if} \PYG{p}{(}\PYG{n}{e}\PYG{p}{.}\PYG{n+na}{getHttpStatus}\PYG{p}{(}\PYG{p}{)} \PYG{o}{=}\PYG{o}{=} \PYG{l+m+mi}{404}\PYG{p}{)} \PYG{p}{\PYGZob{}}
				\PYG{esc}{\LOGGER}\PYG{p}{.}\PYG{n+na}{info}\PYG{p}{(}\PYG{l+s}{\PYGZdq{}}\PYG{l+s}{File \PYGZsq{}\PYGZob{}\PYGZcb{}\PYGZsq{} doesn\PYGZsq{}t exist and will be created}\PYG{l+s}{\PYGZdq{}}\PYG{p}{,} \PYG{n}{fileName}\PYG{p}{)}\PYG{p}{;}
				\PYG{n}{fileExists} \PYG{o}{=} \PYG{k+kc}{false}\PYG{p}{;}
			\PYG{p}{\PYGZcb{}} \PYG{k}{else} \PYG{p}{\PYGZob{}}
				\PYG{k}{throw} \PYG{n}{e}\PYG{p}{;}
			\PYG{p}{\PYGZcb{}}
		\PYG{p}{\PYGZcb{}}
		\PYG{n}{CommitAction} \PYG{n}{commitAction} \PYG{o}{=} \PYG{k}{new} \PYG{n}{CommitAction}\PYG{p}{(}\PYG{p}{)}
			\PYG{p}{.}\PYG{n+na}{withFilePath}\PYG{p}{(}\PYG{n}{fileName}\PYG{p}{)}
			\PYG{p}{.}\PYG{n+na}{withContent}\PYG{p}{(}\PYG{n}{fileContent}\PYG{p}{)}
			\PYG{p}{.}\PYG{n+na}{withAction}\PYG{p}{(}\PYG{n}{fileExists} \PYG{o}{?} \PYG{n}{CommitAction}\PYG{p}{.}\PYG{n+na}{Action}\PYG{p}{.}\PYG{esc}{\p{UPDATE}} \PYG{p}{:} \PYG{n}{CommitAction}\PYG{p}{.}\PYG{n+na}{Action}\PYG{p}{.}\PYG{esc}{\p{CREATE}}\PYG{p}{)}\PYG{p}{;}
		
		\PYG{esc}{\gitLabApi}\PYG{p}{.}\PYG{n+na}{getCommitsApi}\PYG{p}{(}\PYG{p}{)}\PYG{p}{.}\PYG{n+na}{createCommit}\PYG{p}{(}
			\PYG{esc}{\gitLabProperties}\PYG{p}{.}\PYG{n+na}{getRepoPath}\PYG{p}{(}\PYG{p}{)}\PYG{p}{,}
			\PYG{k}{new} \PYG{n}{CommitPayload}\PYG{p}{(}\PYG{p}{)}
				\PYG{p}{.}\PYG{n+na}{withBranch}\PYG{p}{(}\PYG{n}{branchName}\PYG{p}{)}
				\PYG{p}{.}\PYG{n+na}{withStartBranch}\PYG{p}{(}\PYG{esc}{\gitLabProperties}\PYG{p}{.}\PYG{n+na}{getStartBranch}\PYG{p}{(}\PYG{p}{)}\PYG{p}{)}
				\PYG{p}{.}\PYG{n+na}{withCommitMessage}\PYG{p}{(}\PYG{n}{commitMessage}\PYG{p}{)}
				\PYG{p}{.}\PYG{n+na}{withAction}\PYG{p}{(}\PYG{n}{commitAction}\PYG{p}{)}
		\PYG{p}{)}\PYG{p}{;}
	\PYG{p}{\PYGZcb{}}
	
	\PYG{k+kd}{public} \PYG{n}{MergeRequest} \PYG{n+nf}{createMergeRequest}\PYG{p}{(}\PYG{n}{String} \PYG{n}{sourceBranch}\PYG{p}{)} \PYG{k+kd}{throws} \PYG{n}{GitLabApiException} \PYG{p}{\PYGZob{}}
		\PYG{esc}{\LOGGER}\PYG{p}{.}\PYG{n+na}{info}\PYG{p}{(}\PYG{l+s}{\PYGZdq{}}\PYG{l+s}{Creating merge request for branch \PYGZsq{}\PYGZob{}\PYGZcb{}\PYGZsq{}}\PYG{l+s}{\PYGZdq{}}\PYG{p}{,} \PYG{n}{sourceBranch}\PYG{p}{)}\PYG{p}{;}
		\PYG{n}{String} \PYG{n}{title} \PYG{o}{=} \PYG{n}{sourceBranch}\PYG{p}{.}\PYG{n+na}{toUpperCase}\PYG{p}{(}\PYG{p}{)}\PYG{p}{;}
		\PYG{k}{return} \PYG{esc}{\gitLabApi}\PYG{p}{.}\PYG{n+na}{getMergeRequestApi}\PYG{p}{(}\PYG{p}{)}\PYG{p}{.}\PYG{n+na}{createMergeRequest}\PYG{p}{(}
			\PYG{esc}{\gitLabProperties}\PYG{p}{.}\PYG{n+na}{getRepoPath}\PYG{p}{(}\PYG{p}{)}\PYG{p}{,}
			\PYG{k}{new} \PYG{n}{MergeRequestParams}\PYG{p}{(}\PYG{p}{)}
				\PYG{p}{.}\PYG{n+na}{withSourceBranch}\PYG{p}{(}\PYG{n}{sourceBranch}\PYG{p}{)}
				\PYG{p}{.}\PYG{n+na}{withTargetBranch}\PYG{p}{(}\PYG{esc}{\gitLabProperties}\PYG{p}{.}\PYG{n+na}{getStartBranch}\PYG{p}{(}\PYG{p}{)}\PYG{p}{)}
				\PYG{p}{.}\PYG{n+na}{withTitle}\PYG{p}{(}\PYG{n}{title}\PYG{p}{)}
				\PYG{p}{.}\PYG{n+na}{withRemoveSourceBranch}\PYG{p}{(}\PYG{k+kc}{true}\PYG{p}{)}
		\PYG{p}{)}\PYG{p}{;}
	\PYG{p}{\PYGZcb{}}
	
	\PYG{k+kd}{public} \PYG{k+kt}{boolean} \PYG{n+nf}{checkIfMerged}\PYG{p}{(}\PYG{k+kt}{int} \PYG{n}{mergeRequestIid}\PYG{p}{)} \PYG{k+kd}{throws} \PYG{n}{GitLabApiException} \PYG{p}{\PYGZob{}}
		\PYG{k}{return} \PYG{l+s}{\PYGZdq{}}\PYG{l+s}{merged}\PYG{l+s}{\PYGZdq{}}\PYG{p}{.}\PYG{n+na}{equalsIgnoreCase}\PYG{p}{(}
			\PYG{esc}{\gitLabApi}\PYG{p}{.}\PYG{n+na}{getMergeRequestApi}\PYG{p}{(}\PYG{p}{)}\PYG{p}{.}\PYG{n+na}{getMergeRequest}\PYG{p}{(}\PYG{esc}{\gitLabProperties}\PYG{p}{.}\PYG{n+na}{getRepoPath}\PYG{p}{(}\PYG{p}{)}\PYG{p}{,} \PYG{n}{mergeRequestIid}\PYG{p}{)}\PYG{p}{.}\PYG{n+na}{getState}\PYG{p}{(}\PYG{p}{)}
		\PYG{p}{)}\PYG{p}{;}
	\PYG{p}{\PYGZcb{}}
	
	\PYG{k+kd}{public} \PYG{k+kt}{boolean} \PYG{n+nf}{checkIfOpen}\PYG{p}{(}\PYG{k+kt}{int} \PYG{n}{mergeRequestIid}\PYG{p}{)} \PYG{k+kd}{throws} \PYG{n}{GitLabApiException} \PYG{p}{\PYGZob{}}
		\PYG{k}{return} \PYG{l+s}{\PYGZdq{}}\PYG{l+s}{opened}\PYG{l+s}{\PYGZdq{}}\PYG{p}{.}\PYG{n+na}{equalsIgnoreCase}\PYG{p}{(}
			\PYG{esc}{\gitLabApi}\PYG{p}{.}\PYG{n+na}{getMergeRequestApi}\PYG{p}{(}\PYG{p}{)}\PYG{p}{.}\PYG{n+na}{getMergeRequest}\PYG{p}{(}\PYG{esc}{\gitLabProperties}\PYG{p}{.}\PYG{n+na}{getRepoPath}\PYG{p}{(}\PYG{p}{)}\PYG{p}{,} \PYG{n}{mergeRequestIid}\PYG{p}{)}\PYG{p}{.}\PYG{n+na}{getState}\PYG{p}{(}\PYG{p}{)}
		\PYG{p}{)}\PYG{p}{;}
	\PYG{p}{\PYGZcb{}}
	
	\PYG{k+kd}{public} \PYG{k+kd}{static} \PYG{n}{String} \PYG{n+nf}{convertStringToBranchName}\PYG{p}{(}\PYG{n}{String} \PYG{n}{s}\PYG{p}{)} \PYG{p}{\PYGZob{}}
		\PYG{c+c1}{//\textit{https://wincent.com/wiki/Legal\_Git\_branch\_names}}
		\PYG{k}{return} \PYG{n}{s}\PYG{p}{.}\PYG{n+na}{replaceAll}\PYG{p}{(}\PYG{l+s}{\PYGZdq{}}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{s}\PYG{l+s}{\PYGZdq{}}\PYG{p}{,} \PYG{l+s}{\PYGZdq{}}\PYG{l+s}{\PYGZus{}}\PYG{l+s}{\PYGZdq{}}\PYG{p}{)}
			\PYG{p}{.}\PYG{n+na}{replaceAll}\PYG{p}{(}\PYG{l+s}{\PYGZdq{}}\PYG{l+s}{\PYGZca{}}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{.|}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{.}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{.|}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{|[}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{000\PYGZhy{}}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{037}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{0177*\PYGZti{}\PYGZca{}:?}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{[]|.lock\PYGZdl{}|@}\PYG{l+s}{\PYGZbs{}\PYGZbs{}}\PYG{l+s}{\PYGZob{}}\PYG{l+s}{\PYGZdq{}}\PYG{p}{,} \PYG{l+s}{\PYGZdq{}}\PYG{l+s}{\PYGZdq{}}\PYG{p}{)}
			\PYG{p}{.}\PYG{n+na}{toLowerCase}\PYG{p}{(}\PYG{p}{)}\PYG{p}{;}
	\PYG{p}{\PYGZcb{}}
\PYG{p}{\PYGZcb{}}
\end{Verbatim}
